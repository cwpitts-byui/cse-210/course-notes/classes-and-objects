TEX       := lualatex
BIB       := bibtex
BIBFLAGS  := 
TEXFLAGS  := -shell-escape
FILENAME  := classes-and-objects
VIEWER    := zathura

.PHONY: clean
.DEFAULT_TARGET: all

all: ${FILENAME}.pdf

%.pdf:%.tex
	${TEX} ${TEXFLAGS} ${FILENAME}
	${BIB} ${BIBFLAGS} ${FILENAME} || true
	${TEX} ${TEXFLAGS} ${FILENAME} 2>&1 >/dev/null
	${TEX} ${TEXFLAGS} ${FILENAME} 2>&1 >/dev/null

clean:
	rm -rf *.aux *.log *.toc *.idx *.pdf *.out
