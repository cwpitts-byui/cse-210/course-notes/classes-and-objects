\documentclass{beamer}
\usetheme{metropolis}

\usepackage{nameref, minted}

% \makeatletter
% \newcommand*{\currentname}{\@currentlabelname}
% \makeatother

\author{Christopher Pitts}
\title{Classes and Objects}
\date{CSE 210}

\begin{document}
\maketitle

\begin{frame}{Overview}
  \tableofcontents
\end{frame}

\section{What Problem Do Classes Solve?}
\begin{frame}{Picture This}
  You have data structures that contain data (naturally). You also
  have functions that operate on those data
  structures. Problematically, those functions and data structures can
  be updated independently, and it is very easy to update a structure
  and forget to update the corresponding class. They might not even be
  in the same file, which compounds the problem.
\end{frame}

\begin{frame}{Picture This}
  Wouldn't it be nice if you could reason about the data structures
  and the functions that operate on them as a logical unit? And
  furthermore, wouldn't it be nice if some parts of that data could
  \emph{only} be modified by the functions that were supposed to be
  modifying them, so you couldn't even do it accidentally?
\end{frame}

\section{What Is A Class?}
\begin{frame}{Definition}
  A \emph{class} is the unification of data structures and the methods
  that operate on them, usually with some level of access control that
  prevents unintended (or well-intentioned but misguided) modification
  of the data.
\end{frame}

\begin{frame}{Solving A Problem}
  Classes provide a solution to the problem statement from previous
  slides. Because a class describes both data structures and the
  functions that operate on them, you can reason about them
  together. And, if your language is statically typed/compiled, you
  can know when you've forgotten to update the functions and/or the
  data structures!
\end{frame}

\section{Python Classes}
\begin{frame}{Why Do Python Classes Behave The Way They Do?}
  Python classes have some interesting semantics:

  \begin{enumerate}
  \item Required use of ``self'' as the first parameter\footnote{There
      are exceptions to this, and it doesn't have to be called
      ``self''.}
  \item On-the-fly attribute definition
  \item Complete lack of privacy
  \end{enumerate}
\end{frame}
\begin{frame}{Why Do Python Classes Behave The Way They Do?}
  To really understand this behavior, we need to start with C
  programming.
\end{frame}

\begin{frame}{The Old Days: C Programming}
  C is an old programming language (but still very useful and
  important, you should spend the time to learn it). Notably, it
  predates the dominance of object-oriented programming languages, and
  does not have classes at all.
\end{frame}

\begin{frame}{Now C Here}
  Some important characteristics of C for this discussion:

  \begin{enumerate}
  \item Statically typed
  \item Compiled
  \item Structs: collections of data structures
  \item Functions
  \end{enumerate}
\end{frame}

\begin{frame}{Solving A Problem, The C Way}
  C programmers were aware of the problem addressed
  earlier\footnote{Being C programmers, many of them pretended the
    problem didn't exist as long as it wasn't affecting them.}. So, to
  solve this problem, they defined a convention.
\end{frame}

\begin{frame}{Meet Struct, The C Class}
  \inputminted{c}{listings/c_struct.h}

  A \emph{struct} is a collection of data structures. The first part
  of the convention is that structs contain all of the data you want
  to keep together.
\end{frame}

\begin{frame}{One Struct, Please}
  \inputminted[fontsize=\tiny]{c}{listings/c_methods.c}

  The second part of the convention is that functions that are
  supposed to operate on the data structures are named using the name
  of the struct type (usually), and take as the first parameter an
  instance of the struct.
\end{frame}

\begin{frame}{One Struct, Coming Up}
  \inputminted[fontsize=\tiny, firstline=13, lastline=17]{c}{listings/c_methods.c}

  This function is worth a second look; it's analogous to a
  constructor in other programming languages, taking in the initial
  values and setting them.
\end{frame}

\begin{frame}{From One Good Thing Comes Another}
  Python is implemented in C\footnote{In fact, the reference
    implementation is called CPython; that's the version you install
    when you get it from python.org}, and therefore takes a lot of
  design cues and implementation details from that language.
\end{frame}

\begin{frame}{Look In The Mirror}
  \begin{minipage}[h]{0.49\textwidth}
    \begin{figure}[h]
      \inputminted[fontsize=\tiny]{c}{listings/c_comparison_init.c}
    \end{figure}
    \begin{figure}[h]
      \inputminted[fontsize=\tiny]{python}{listings/python_comparison_init.py}
    \end{figure}
  \end{minipage}
  \begin{minipage}[h]{0.49\textwidth}
    \flushright
    Do these two methods look similar to you?
  \end{minipage}
\end{frame}

\begin{frame}{Python Classes Are Just Fancy C Structs}
  Python classes work exactly as C structs work.

  \begin{enumerate}
  \item That ``self'' parameter is just an instance of the class type
  \item You set all the values in the init method because that's how
    you would do it in C
  \item Python made a conscious choice to do this, but it's very C to
    say ``all the data is public, and we're all consenting
    adults''\footnote{Literally how one person explained it on Stack
      Overflow}
  \end{enumerate}
\end{frame}

\begin{frame}{But Wait...}
  You're probably thinking about the fact that Python doesn't look
  quite like this:

  \inputminted{c}{listings/c_struct_func_call.c}
\end{frame}

\begin{frame}{Pour Some Sugar On That Struct}
  Python uses some \emph{syntactic sugar} to simplify the process of
  writing classes this way. These are equivalent:

  \inputminted[fontsize=\tiny]{python}{listings/python_sugar.py}

  QED: Python Classes Are Just Fancy C Structs
\end{frame}

\begin{frame}{Man, Kipling Was A Visionary}
  This convention happens to be explicitly codified in a modern
  programming language that debuted to much acclaim: Golang does this
  type of method resolution as well. And in fact compels you to write
  out code exactly how you would in C, because Golang doesn't have
  classes\footnote{This was apparently on purpose.}.
\end{frame}

\begin{frame}{Man, Kipling Was A Visionary}
  \inputminted{go}{listings/go_comparison.go}

  After fifty years, we went from C to... C with garbage
  collection\footnote{Yes, yes, I know, Go has other nice
    features.}\footnote{And mandatory \emph{tab} indentation.}?
\end{frame}

\begin{frame}{Coda: What About Being Able To Create New Fields At Runtime?}
  \inputminted{python}{listings/python_runtime_fields.py}
  
  Yeah, this is kind of weird. Python uses what's called the
  ``metaobject protocol'', which is a system for creating and
  modifying classes and objects on the fly. While you \emph{can} do
  this, you really don't want to, it will cause you problems later on
  down the road.
\end{frame}

\end{document}