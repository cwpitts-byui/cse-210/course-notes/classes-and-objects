#include "foo.h"

void fooBazTheBar(struct Foo* f)
{
  f->bar *= f->baz;
}

void fooUpTheBar(struct Foo* f, int u)
{
  f->bar += u;
}

void fooInit(struct Foo* f, int bar, float baz)
{
  f->bar = bar;
  f->baz = baz;
}
