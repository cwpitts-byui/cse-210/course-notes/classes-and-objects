type Foo struct {
	foo int
	bar float
}

func (Foo f) bazTheBar() {
	f.foo *= f.baz
}
