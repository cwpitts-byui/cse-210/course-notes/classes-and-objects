class Foo:
    def __init__(self, bar, baz):
        self.bar = bar
        self.baz = baz

    def baz_the_bar(self):
        self.bar *= self.baz

f = Foo(1, 0.5)
f.baz_the_bar()
Foo.baz_the_bar(f)
